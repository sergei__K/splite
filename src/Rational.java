import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Rational {
    private int num;
    private int denum;

    public Rational(int num, int denum) {
        this.num = num;
        this.denum = denum;
    }

    public Rational() {
        this.num = 1;
        this.denum = 1;
    }

    public void Split(String stroka) {
        String[] mass = new String[2];
        int i = 0;
        for (String retval : stroka.split("/")) {
            mass[i] = retval;
            i++;
        }
        this.num = Integer.parseInt(mass[0]);
        this.denum = Integer.parseInt(mass[1]);
    }

    public Rational addition (Rational rational){
        Rational rational2 = new Rational();
        if (rational.denum==this.denum){
            rational2.num=this.num+rational.num;
            rational2.denum=rational.denum;
        }else {
            rational2.num = this.num * rational.denum + rational.num * this.denum;
            rational2.denum = this.denum * rational.denum;
        }
        return rational2;
    }
    public Rational subtraction (Rational rational){
        Rational rational2 = new Rational();
        if (rational.denum==this.denum){
            rational2.num=this.num-rational.num;
            rational2.denum=rational.denum;
        }else {
            rational2.num = this.num * rational.denum - rational.num * this.denum;
            rational2.denum = this.denum * rational.denum;
        }
        return rational2;
    }
    public static int gcd(int a,int b) {
        while (b !=0) {
            int tmp = a%b;
            a = b;
            b = tmp;
        }
        return a;
    }
    public Rational multiplication (Rational rational){
        Rational rational2 = new Rational();
        rational2.num=this.num*rational.num;
        rational2.denum=this.denum*rational.denum;
        return rational2;
    }
    public Rational division (Rational rational){
        Rational rational2 = new Rational();
        rational2.num=this.num*rational.denum;
        rational2.denum=this.denum*rational.num;
        return rational2;
    }
    public boolean Error(String stroka) {
        Pattern pattern = Pattern.compile("[-]*[1-9][0-9]*[/]*[-]*[1-9]*[0-9]*\\s[+:*-]*\\s[-]*[1-9][0-9]*[/]*[-]*[1-9]*[0-9]");
        Matcher matcher = pattern.matcher(stroka);
        return matcher.find();
    }

    @Override
    public String toString() {
        return num + "/" + denum;
    }


}