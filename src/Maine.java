import java.io.*;
import java.util.Scanner;

public class Maine {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] arg) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\input.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\output.txt"));
        BufferedWriter bufferedWriter1 = new BufferedWriter(new FileWriter("src\\error.txt"));
        Rational rational = new Rational();
        Rational rational2 = new Rational();
        String stroka;
        String[] strings = new String[0];
                while ((stroka = bufferedReader.readLine()) != null) {
                    try {
                        strings = stroka.split(" ");
                        rational.Split(strings[0]);
                        rational2.Split(strings[2]);
                    }catch (NumberFormatException e){
                        File error = new File("src\\error.txt");
                        bufferedWriter1.write(stroka + "\n" + e + "\n");
                    }
                    switch (strings[1]) {
                        case "+":
                            System.out.println(rational.addition(rational2));
                            bufferedWriter.write(String.valueOf(rational.addition(rational2))+"\n");
                            break;
                        case "-":
                            System.out.println(rational.subtraction(rational2));
                            bufferedWriter.write(String.valueOf(rational.subtraction(rational2))+"\n");
                            break;
                        case ":":
                            System.out.println(rational.division(rational2));
                            bufferedWriter.write(String.valueOf(rational.division(rational2))+"\n");
                            break;
                        case "*":
                            System.out.println(rational.multiplication(rational2));
                            bufferedWriter.write(String.valueOf(rational.multiplication(rational2))+"\n");
                            break;
                    }
                }
        bufferedWriter1.close();
        bufferedReader.close();
        bufferedWriter.close();
    }
}